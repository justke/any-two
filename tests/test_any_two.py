from unittest import TestCase

from src.any_two import any_two, any_two_friend


class TestAnyTwo(TestCase):

    def test_happy_path(self):
        self.assertTrue(any_two([15, 10, 7, 3], 17))

    def test_empty_list_is_handled(self):
        self.assertFalse(any_two([], 7))

    def test_ordered_list(self):
        self.assertTrue(any_two([1, 2, 3, 4], 5))

    def test_sum_not_found(self):
        self.assertFalse(any_two([1, 2, 3, 4], 9))

    def test_sum_made_with_one_item_not_two(self):
        self.assertFalse(any_two([10, 15, 3, 7], 7))

    def test_duplicates_in_list(self):
        self.assertTrue(any_two([1, 1, 3, 7], 2))


class TestAnyTwoLearned(TestCase):

    def test_happy_path(self):
        self.assertTrue(any_two_friend([15, 10, 7, 3], 17))

    def test_empty_list_is_handled(self):
        self.assertFalse(any_two_friend([], 7))

    def test_ordered_list(self):
        self.assertTrue(any_two_friend([1, 2, 3, 4], 5))

    def test_sum_not_found(self):
        self.assertFalse(any_two_friend([1, 2, 3, 4], 9))

    def test_sum_made_with_one_item_not_two(self):
        self.assertFalse(any_two_friend([10, 15, 3, 7], 7))

    def test_duplicates_in_list(self):
        self.assertTrue(any_two_friend([1, 1, 3, 7], 2))
