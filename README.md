# Any Two Code Challenge

Given a list of numbers and a number k, return whether any two numbers from the list add up to k.

E.g. given [10, 15, 3, 7] and k of 17, return true due to the two elements 10 and 7 adding up to 17.

```python
>>> any_two([10, 15, 3, 7], 17)
True

>>> any_two([], 10)
False

>>> any_two([1, 2, 3, 4], 5)
True

>>> any_two([1, 2, 3, 4], 9)
False
```
