from typing import List
from itertools import combinations


def any_two(items: List[int], total: int) -> bool:
    """
    Given a list of numbers and a number k, return whether any two numbers from the list add up to k.

    :param items: (list) - The list of numbers to check.
    :param total: (int) - The sum to look for between two numbers in items.
    :return: (bool) - Whether or not the total can be made from adding two numbers from items.
    """
    if not items:
        return False
    first, *new_items = items
    if total - first in new_items:
        return True
    return any_two(new_items, total)


def any_two_friend(items: List[int], total: int) -> bool:
    """
    Solution from a friend to be kept in mind (one slight modification)
    """
    return any(a + b == total for a, b in combinations(items, 2))
